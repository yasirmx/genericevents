﻿namespace Events.Library
{
    using System;

    public class EventRepository<T> where T: class
    {
        public event EventHandler<T> EventHandler;

        public event EventHandler OnCompleted;

        public void OnEventExecuting(T EventData)
        {
            EventHandler(this, EventData);

            OnCompleted(this, EventArgs.Empty);
        }
    }
}
