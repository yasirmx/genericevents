﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Events.Library
{
    public class EventRepositoryArgs : EventArgs
    {
        public string EventName { get; set; }
        public string Value { get; set; }
        
    }


}
