﻿namespace Events.Display
{
    using System;
    using Events.Library;

    class Program
    {
        static void Main(string[] args)
        {

            var myEvent = new EventRepository<EventRepositoryArgs>();

            myEvent.EventHandler += MyEvent_EventHandler;
            myEvent.OnCompleted += MyEvent_OnCompleted;
            var eventArgs = new EventRepositoryArgs { Value = 25.ToString(), EventName = "twenty five" };
            myEvent.OnEventExecuting(eventArgs);
            

        }

        private static void MyEvent_OnCompleted(object sender, EventArgs e)
        {
            Console.WriteLine("Execution completed");
            Console.ReadLine();
        }

        private static void MyEvent_EventHandler(object sender, EventRepositoryArgs e)
        {
            Console.WriteLine("Value is : {0} and EventName is {1} ", e.Value, e.EventName);
        }

    }
}
